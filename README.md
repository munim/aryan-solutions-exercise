# aryan-solutions-exercise

### Author: Abdul Munim

Start time: 27.02.2019 12:20 (BST)

### How to execute the solution

```
$ mvn exec:java
```

### Answer 1

**Can you implement the sing() method for the bird?**
> Implemented sing as a strategy.

**a. How did you unit test it?**
> Tested each behaviour i.e. walk, fly and sing individually.

**b. How did you optimize the code for maintainability?**
> Since sing, walk, etc are behaviour of the animal class, in my opinion its better to use Strategy pattern to easily extend the behaviours for other inherited classes.
>
> Help taken from:
> https://www.dofactory.com/net/strategy-design-pattern


### Answer 2

**Now, we have 2 special kinds of birds: the Duck and the Chicken... Can you implement them to make their own special sound?
a. A duck says: “Quack, quack”**
> Implemented `DuckSayStategy`

**b. A duck can swim**
> Implemented `Swimable` interface

**c. A chicken says: “Cluck, cluck”**
> Implemented `ChickenSayStategy`

**d. A chicken cannot fly (assumption: its wings are clipped but ignore that)**
> Implemented `CannotFlyStrategy`

### Answer 3

**Now how would you model a rooster?
**a. A rooster says: “Cock-a-doodle-doo”**
> Implemented `RoosterSayStrategy`

**b. How is the rooster related to the chicken?**
> Chicken is not gender specific, Rooster is male.
> Source: Google

**c. Can you think of other ways to model a rooster without using inheritance?**
> Through Factory pattern having gender attribute in `Chicken`

**Answer 4**
**Can you model a parrot? We are specifically interested in three parrots, one that lived in a house with dogs one in a house with cats, the other lived on a farm next to the rooster.
a. A parrot living with dogs says: “Woof, woof”**
> As per my understanding, a better approach is to do with Decorator pattern. Implemented `ParrotDecorator`. I also could be done with Factory pattern with tweaks.

**b. A parrot living with cats says: “Meow”**

> Implemented in `ParrotDecorator`.

**c. A parrot living near the rooster says: “Cock-a-doodle-doo”**
> Implemented in `ParrotDecorator`.

**d. How do you keep the parrot maintainable? What if we needanother parrot
lives near a Duck? Or near a phone that rings frequently?**

> Since I have used the Decorator pattern, only thing is to know the class that says near and modify the `ParrotDecorator` class.

  

>  ***NOTE:** I have ran out of time and unable to implement the rest. Most of the time was taken by analysing the questions and find out a better way of implementing it.*


### Answers below would be way I would implement it

**B. Model fish as well as other swimming animals**

1. In addition to the birds, can you model a fish?
a. Fishes don’t sing


> I would refactor all `Animal` classes and implement it's behaviour,
> such as `Speakable`, `Singable`, `Walkable`, `Swimable` etc. `Fish`
> class would `implements` these classes. I have done this for `Duck`
> class which is `Swimable`

b. Fishes don’t walk
c. Fishes can swim

> for **b and c**, it is easily extendable and maintainable.

2. Can you specialize the fish as a Shark and as a Clownfish?
a. Sharks are large and grey
b. Clownfish are small and colourful (orange)
c. Clownfish make jokes
d. Sharks eat other fish

> For **a, b, c and d**, I would implement Factory pattern i.e.
>  `FishFactory` and initialise the attributes for `Shark` and
>  `Clownflish`


3. Dolphins are not exactly fish, yet, they are good swimmers
a. Can you model a dolphin that swims without inheriting from a fish class?
b. How do you avoid duplicating code or introducing unneeded overhead?
  

>  `Dolphin` class can be implemented with Composite pattern without inheriting from `Fish` class and with less code duplication and overhead.

  

**D. Model animals that change their behaviour over time**
1. Can you model a butterfly?
a. A butterfly can fly**
b. A butterfly does not make a sound

> I would set the `FlyStrategy` and remove `SayStrategy` for
>  `Butterfly` class.

  

2. Can you optimize your model to account for the metamorphosis from caterpillar to butterfly?
a. A caterpillar cannot fly
b. A caterpillar can walk (crawl)

> I would inherit `Caterpiller` class from `Butterfly` and remove
>  `FlyStrategy` and add `WalkStrategy`


E. Counting animals
 

> Since I would have refactored all the classes implementing `Flyable`,
>  `Walkable`, `Singable` and `Swimable`. I would then iterate through
> the list of `Animal` and check for the implementations and do a count
> on those each.