package exercise.models.behaviours;

public interface Swimable {
    void swim();
}
