package exercise.models;

import exercise.models.strategies.Strategy;
import exercise.models.strategies.WalkStrategy;

public class Animal {
    // Since these are attributes hence it's easier to maintain with Strategy pattern
    Strategy walkStrategy = new WalkStrategy();

    public void walk() {
        walkStrategy.perform();
    }
}
