package exercise.models;

import exercise.models.behaviours.Swimable;
import exercise.models.strategies.DuckSayStrategy;

public class Duck extends Bird implements Swimable {

    public Duck() {
        setSayStrategy(new DuckSayStrategy());
    }

    @Override
    public void swim() {
        System.out.println("I can swim");

    }
}
