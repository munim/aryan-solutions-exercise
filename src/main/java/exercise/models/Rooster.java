package exercise.models;

import exercise.models.strategies.RoosterSayStrategy;

public class Rooster extends Bird {
    public Rooster() {
        setSayStrategy(new RoosterSayStrategy());
    }
}
