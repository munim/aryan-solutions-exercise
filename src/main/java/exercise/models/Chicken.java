package exercise.models;

import exercise.models.strategies.CannotFlyStategy;
import exercise.models.strategies.ChickenSayStrategy;

public class Chicken extends Bird {

    public Chicken() {
        setFlyStrategy(new CannotFlyStategy());
        setSayStrategy(new ChickenSayStrategy());
    }
}
