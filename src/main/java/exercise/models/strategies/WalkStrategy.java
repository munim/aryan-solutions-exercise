package exercise.models.strategies;

public class WalkStrategy implements Strategy {
    @Override
    public void perform() {
        System.out.println("I am walking");
    }
}
