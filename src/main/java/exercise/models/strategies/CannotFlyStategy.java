package exercise.models.strategies;

public class CannotFlyStategy implements Strategy {
    @Override
    public void perform() {
        System.out.println("Wings are clipped, cannot fly");

    }
}
