package exercise.models.strategies;

public class ChickenSayStrategy implements Strategy {
    @Override
    public void perform() {
        System.out.println("Cluck, cluck");

    }
}
