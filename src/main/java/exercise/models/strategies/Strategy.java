package exercise.models.strategies;

public interface Strategy {
    void perform();
}
