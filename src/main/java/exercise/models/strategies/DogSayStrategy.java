package exercise.models.strategies;

public class DogSayStrategy implements Strategy {
    @Override
    public void perform() {
        System.out.println("Woof, woof");

    }
}
