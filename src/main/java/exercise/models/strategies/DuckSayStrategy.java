package exercise.models.strategies;

public class DuckSayStrategy implements Strategy {
    @Override
    public void perform() {
        System.out.println("Quack, quack");
    }
}
