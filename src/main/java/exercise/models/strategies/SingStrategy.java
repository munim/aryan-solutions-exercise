package exercise.models.strategies;

public class SingStrategy implements Strategy {

    @Override
    public void perform() {
        System.out.println("I am singing");
    }
}
