package exercise.models;

import exercise.models.strategies.FlyStrategy;
import exercise.models.strategies.SayStrategy;
import exercise.models.strategies.SingStrategy;
import exercise.models.strategies.Strategy;

public class Bird extends Animal {
    private Strategy flyStrategy = new FlyStrategy();
    private Strategy singStrategy = new SingStrategy();
    private Strategy sayStrategy = new SayStrategy();

    public void fly() {
        flyStrategy.perform();
    }

    public void sing() {
        singStrategy.perform();
    }

    public void say() {
        sayStrategy.perform();
    }

    protected void setFlyStrategy(Strategy flyStrategy) {
        this.flyStrategy = flyStrategy;
    }

    protected void setSingStrategy(Strategy singStrategy) {
        this.singStrategy = singStrategy;
    }

    protected void setSayStrategy(Strategy sayStrategy) {
        this.sayStrategy = sayStrategy;
    }
}
