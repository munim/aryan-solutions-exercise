package exercise;

import exercise.decorators.ParrotDecorator;
import exercise.models.*;

/**
 * Hello world!
 */
public class Solution {
    public static void main(String[] args) {

        System.out.println("Answer 1:");
        Bird bird = new Bird();
        bird.walk();
        bird.fly();
        bird.sing();
        System.out.println("---------------------");

        System.out.println("Answer 2:");
        Bird duck = new Duck();
        duck.say();
        ((Duck) duck).swim();
        Bird chicken = new Chicken();
        chicken.say();
        chicken.fly();
        System.out.println("---------------------");

        System.out.println("Answer 3:");
        Bird rooster = new Rooster();
        rooster.say();
        System.out.println("---------------------");

        System.out.println("Answer 4:");
        Bird parrotLivingWithDog = ParrotDecorator.LivesWith(Dog.class);
        parrotLivingWithDog.say();

        Bird parrotLivingWithCat = ParrotDecorator.LivesWith(Cat.class);
        parrotLivingWithCat.say();

        Bird parrotLivingWithRooster = ParrotDecorator.LivesWith(Rooster.class);
        parrotLivingWithRooster.say();
        System.out.println("---------------------");

    }
}
