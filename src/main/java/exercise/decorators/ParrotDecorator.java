package exercise.decorators;

import exercise.models.Bird;
import exercise.models.Parrot;
import exercise.models.strategies.CatSayStrategy;
import exercise.models.strategies.DogSayStrategy;
import exercise.models.strategies.RoosterSayStrategy;

public class ParrotDecorator {
    public static Bird LivesWith(Class className) {
        Bird parrot = new Parrot();
        if (className.getName().contains("Dog")) {
            ((Parrot) parrot).setSayStrategy(new DogSayStrategy());
        } else if (className.getName().contains("Cat")) {
            ((Parrot) parrot).setSayStrategy(new CatSayStrategy());
        } else if (className.getName().contains("Rooster")) {
            ((Parrot) parrot).setSayStrategy(new RoosterSayStrategy());
        }
        return parrot;
    }
}
